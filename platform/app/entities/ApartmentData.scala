package entities

case class ApartmentData(
    area: BigDecimal,
    district: String,
    metro: String,
    wallType: String,
    city: String)
